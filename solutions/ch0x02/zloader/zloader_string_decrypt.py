import sys
from binaryninja import *

def decrypt_strings(encrypted_string, xor_key):
    xor = Transform["XOR"]
    decrypted = xor.encode(encrypted_string, {"key":xor_key})

    return decrypted

def main():
    db = sys.argv[1]
    bv = BinaryViewType.get_view_of_file(db)
    bv.update_analysis()

    br = BinaryReader(bv)
    bw = BinaryWriter(bv)

    mw_string_decrypt_addr = 0x1104ff0
    decryption_key_addr = 0x11084d0

    br.seek(decryption_key_addr)
    
    # Key: "hZ'12WV2#0KM1heE"
    xor_key = br.read(16)
    
    # Get all the xrefs to string decrypt function
    xrefs = bv.get_code_refs(mw_string_decrypt_addr)

    for xref in xrefs:
        # Use Medium Level IL to extract the arguments to the mw_string_decrypt function
        # and decrypt the strings
        il = xref.function.get_low_level_il_at(xref.address).mlil
        
        if il.operation == MediumLevelILOperation.MLIL_CALL:
            if il.operands[2][0].value.value != None:
                # This encrypted buffer is at address 0x006483d8 and includes the xor key
                # and we just need the string which is the first 280 bytes.  
                if il.operands[2][0].value.value == 0x11083d8:
                    br.seek(0x11083d8)
                    current_data_var = br.offset
                    encrypted_string = br.read(243)
                    decrypted_string = decrypt_strings(encrypted_string, xor_key)
                    print("{0}-->{1}".format(hex(current_data_var), decrypted_string))
                    bv.write(current_data_var, decrypted_string)
                
                br.seek(il.operands[2][0].value.value)
                
                # Determine the size of encrypted data stored at the current data variable
                current_data_var = br.offset
                next_data_var = bv.get_next_data_var_after(current_data_var)
                size = next_data_var.address - current_data_var
                
                encrypted_string = br.read(size)
                decrypted_string = decrypt_strings(encrypted_string, xor_key)
                bv.write(current_data_var, decrypted_string)
                print("{0}-->{1}".format(hex(current_data_var), decrypted_string))

    bv.save_auto_snapshot()

if __name__ == "__main__":
    main()