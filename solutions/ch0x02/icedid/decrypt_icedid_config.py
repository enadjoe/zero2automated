#!/usr/bin/env python3

import sys
from binaryninja import  (BinaryViewType, BinaryReader, BinaryWriter, Transform)

def main():
    db = sys.argv[1]
    bv = BinaryViewType.get_view_of_file(db)
    bv.update_analysis_and_wait()

    br = BinaryReader(bv)
    bw = BinaryWriter(bv)
    
    rc4 = Transform["RC4"]

    data_section_addr = 0x403000
    br.seek(data_section_addr)

    key = br.read(8)
    encrypted_config = br.read(0x248) # config is 584 bytes

    decrypted_config = rc4.encode(encrypted_config, {"key":key})
    
    print("===========Decrypted Config============")
    print(decrypted_config)
    print("=======================================")

    # Patch decrypted config and save database to new file
    config_buffer_addr = 0x403008
    bw.seek(config_buffer_addr)
    bw.write(decrypted_config)
    bv.save_auto_snapshot()

if __name__ == "__main__":
    main()